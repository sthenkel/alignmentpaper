#!/python
import glob,os,time


outputDir = "mergedOutput"
inputList = "mergelist"
#fileToMerge = 'CombinedMonitoring'
fileToMerge = 'ZmumuValidation'
#outputFile = "MergedMonitoring.root"
outputFile = "MergedZmumuValidation.root"
NoFilesToMerge = 7651
counter = 0
t0 = time.time()
#-- Let's start --#
os.system("mkdir -p "+str(outputDir))
loc = os.path.abspath('.')
txtfile = open(str(outputDir)+"/"+str(inputList)+".txt","w")
for subdirs,dirs, files in os.walk(loc):
    for file in files:
        if file.find(fileToMerge) != -1:
            fileLoc = os.path.join(subdirs, file)
            counter+=1
            txtfile.write(str(fileLoc)+"\n")
        else:
            continue
txtfile.close()
print 'Number of files', counter
#if counter == NoFilesToMerge:
if counter:
    print 'SUCCESS - number of files to merge is equal to number of files \n'
    os.chdir(loc+"/"+outputDir)
    # run athena
    cmd = "DQHistogramMerge.py "+str(inputList)+".txt %s True" % outputFile
    print "\nRun command:"
    print cmd
    print "\nLogfile:"
    print "------------------------------------------------------------------"
    retcode = os.system(cmd)
    print "------------------------------------------------------------------"
    dt = int(time.time() - t0)

    print "\n## DQHistogramMerge.py finished with retcode = %s" % retcode
    print   "## ... elapsed time: ", dt, " sec"
else:
    print 'WARNING :: Number of files is not equal to the number of expected files to exist'
