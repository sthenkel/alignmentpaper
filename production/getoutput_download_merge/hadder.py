'''

This program takes an input list of .root files separated by \n and hadds
them in groups of numPerHadd at a time.  Or at least that is the idea...

'''


from imp import find_module
try:
    find_module('ROOT')
    found = True
except ImportError:
    found = False

# hadd between 0-500 files.  Outputs will be hadded into one file later.
# (Unless there is only one call, then that will be the final file).
#
# inFiles = list of the full paths to the source root files
# name = full name (with extension!) of the output root file
# start_pos = where in inFiles to start
# end_pos = where in inFiles to end
def hadd(inFiles,name,start_pos,end_pos):
    args = ["hadd", "-f",name] + inFiles[start_pos:end_pos]
    #print inFiles,inFiles[start_pos:end_pos],args
    call(args)

# main function to actually run shit
if __name__ == '__main__':
    if not found:
        print "Module 'ROOT' not found!  You cannot hadd without it, dumbass."
        exit()

    import sys
    from subprocess import call

    # check arguments
    if "-h" in sys.argv or "--help" in sys.argv:
        print "Usage:\npython hadder.py <List of files to hadd> <name of hadded file (omit '.root' extension!)>"
        exit()
    if not len(sys.argv) == 3:
        print "Expected only 2 arguments!  Try 'python hadder.py -h' for usage information"
        exit()

    # everything looks good -- let's get started
    fileList = sys.argv[1]
    outName = sys.argv[2]
    numPerHadd = 500

    inFile = open(fileList,'r')
    files = []
    for line in inFile:
        line = line.strip()
        files.append(line)

    # if we only need one hadd call
    if len(files) <= numPerHadd:
        hadd(files, outName+'.root', 0, len(files))
        print "hadd complete!"
        exit(1)

    # if we need more than one hadd call
    start = 0
    end = numPerHadd
    iter = 1
    temp_hadds = [] # intermediate hadds that will be merged later

    print "before hadd loop", start, end, iter, len(files)
    
    while True:
        hadd(files, outName+"."+str(iter)+'.root', start, end) # do the hadding

        temp_hadds.append(outName+"."+str(iter)+'.root') # store the intermediate file name

        # set variables for next iteration
        start = start + numPerHadd
        if len(files) == end:
            break
        if len(files) < (end + numPerHadd):
            end = len(files)
        else:
            end = end + numPerHadd
        iter = iter + 1

    print temp_hadds

    # hadd the intermediate files into the final file
    hadd(temp_hadds, outName+'.root', 0, len(temp_hadds))

    # delete the intermediate hadds
    for temp_hadd in temp_hadds:
        print "Deleting temporary .root file:",temp_hadd
        call(["rm",temp_hadd])

    print "hadd complete!"
