#!/bin/bash
#setupATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'

setupATLAS

lsetup git
cd athena
#asetup 21.0.21,gcc49,64,here

lsetup panda
#mkdir source build run
#cd source
asetup AtlasOffline,21.0.20,here
#asetup Athena,21.0.50,here

#lsetup panda

cd ..
source sourcing.sh