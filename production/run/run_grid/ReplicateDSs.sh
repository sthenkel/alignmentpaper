#!/bin/bash
#site="CERN-PROD_SCRATCHDISK"
#site="CERN-EXTENSION_SCRATCHDISK"
#site="AGLT2_SCRATCHDISK"
site="DESY-ZN_SCRATCHDISK"
#site="CA-SFU-T2_SCRATCHDISK"
#site="CA-VICTORIA-WESTGRID-T2_SCRATCHDISK"
#site="CA-WATERLOO-T2_SCRATCHDISK"
#site="CERN-PROD_DET-INDET"
path="./"
#for input in `find . -name 'Runs_INIT_data16*.txt'`
for input in `find . -name 'last.txt'`
do
    echo "This is the Inputfile: $input"
    for file in `cat $input`
    do
	echo "Currently replicating : $file to $site"
	rucio add-rule $file --grouping DATASET 1 "$site"
    done

done