#!/bin/python
# author: steffen henkelmann
# prerequists: setup rucio
from __future__ import division
import sys,os,re,subprocess
import glob
from collections import defaultdict




inP = "Runs_INIT_data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml.txt"
inP="Runs_INIT_data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml.txt"
runList=[line.strip() for line in open(inP)]
print runList
totEvents=0.
totTBytes=0.
notAvail=0
totN=0
for item in runList:
    print 'INFO :: Check for > ', item
    ds = subprocess.Popen(['rucio','get-metadata', item], stdout=subprocess.PIPE)
    outds, errds = ds.communicate()
    lines = outds.splitlines()
    print lines
    if not "No" in filter(lambda x: 'events' in x, lines)[0].strip('events: '):
        ev =    float(filter(lambda x: 'events' in x, lines)[0].strip('events: '))
        print 'INFO :: events',   ev
        totEvents+=ev
    else:
        "WARNING :: no event info ..."
        notAvail+=1

    if not "No" in filter(lambda x: 'bytes' in x, lines)[0].strip('bytes: '):
        tb = float(filter(lambda x: 'bytes' in x, lines)[0].strip('bytes: '))/(1e12)
        print 'INFO :: TerraBytes > ', tb
        totTBytes+=tb
    else:
        "WARNING :: no bytes info ..."
    totN+=1

print '\n--------------------------------------'
print '-       '+  str(runList[0])[0:12]  +'          -'
print '--------------------------------------\n'
print 'INFO :: nevents total > ', totEvents
print 'INFO :: tbs total > ', totTBytes
print 'INFO :: total number of data sets > ', totN
print 'INFO :: metedata n/a for > ', notAvail, ' data sets'
